import json
import csv
import re
import dateparser
import datetime
from collections import namedtuple
from keboola import docker

# initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

# access the supplied values
rawtextcol = cfg.get_parameters()['rawtextcol']
rawtermscol = cfg.get_parameters()['rawtermscol']
idcol = cfg.get_parameters()['idcol']
file = cfg.get_parameters()['file']
outfile = cfg.get_parameters()['outfile']


def load_data(file, cols):
    """load data"""
    data = []
    with open(file) as f:
        f_csv = csv.DictReader(f)
        for row in f_csv:
            #print row[cols[1]]
            start, end, contract_end, tagd, tagt = get_dates(row[cols[1]], row[cols[2]])
            extract = row[cols[0]], start, end, contract_end, tagd, tagt
            #print extract
            data.append(extract)
    return data


def save_data(data, file, headers):
    """save data as csv"""
    with open(file,'w') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(headers)
        f_csv.writerows(data)
    pass


def get_contract(text):
    """extract start and end from terms"""
    text = text.upper()
    rawdates = text.split("TO")
    
    dates = []
    #print rawdates
    for a in rawdates:
        edate = dateparser.parse(a)
        dates.append(edate)
    #print dates
    
    try:
        start = min(dates)
        end = max(dates)
        tag = 1
    except:
        start = datetime.date(1900, 01, 01)
        end = datetime.date(2099, 01, 01)
        tag = 0
    #print start, end, tag
    """
    ## test for dates parsed
    try:
        ds = datetime.datetime.strptime(start,'%Y-%m-%d')
        de = datetime.datetime.strptime(end,'%Y-%m-%d')
        tag = 1
    except:
        tag = 0
    """
    return start, end, tag


def get_terms(text):
    """Get terms parameters out of text"""
    text = text.upper()
    text = text.strip("()")
    print text
    ## everything split by "-"
    try:
        rawterms = text.split(" - ")
        #print rawterms
        rterm = rawterms[0]
        ryears = rawterms[1]
        
        ## multiple terms rterm
        
        ## years of term ryears
        try:
            print "ryears RAW: "+ryears
            #ryearsnum = [int(s) for s in str(ryears).split() if s.isdigit()]
            #ryearsnum = filter(str.isdigit, ryears)
            ryearsnumRAW = re.sub(r'\D', "", ryears)
            ryearsnumRAW = ryearsnumRAW[0]
            #print "ryearsnum: "+ryearsnumRAW
            #print type(ryearsnumRAW)
            #print type(ryearsnumRAW[0])
            ryearsnum = ryearsnumRAW[0]
            print "ryearsnum FIN: "+ryearsnum
        except:
            ryearsnum = 0
        
        if ryearsnum==0:
            tag = 0
        else:
            tag = 1
    
    ## Should be extended by more functions
    except:
        tag = 0
        rterm = ""
        ryears = ""
        ryearsnum = ""
    print rterm, ryears, ryearsnum, tag
    return rterm, ryears, ryearsnum, tag


def add_years(d, years):
    try:
        return d.replace(year = d.year + years)
    except ValueError:
        return d + (date(d.year + years, 1, 1) - date(d.year, 1, 1))


def get_dates(rawtext, rawterms):
    tagd = 0
    tagt = 0
    """get all values under one roof, return dates"""
    start, end, tagd = get_contract(rawtext)
    #print start, end, tagd
    
    rterm, ryears, ryearsnum, tagt = get_terms(rawterms)
    #print rterm, ryears, ryearsnum, tagt
    
    ## find contract end
    if tagt==1:
        try:
            extension = int(rterm)*int(ryearsnum)
            print extension
            contract_end = add_years(end, extension)
            #print contract_end
        except:
            #print "Exception 1: CANNOT CALCULATE EXTENSION"
            contract_end = ""
    else:
        #print "Exception 2: NO TERM FOUND"
        contract_end = ""
    
    ## convert to dates only
    try:
        start = start.date()
        start = str(start)
    except:
        start = ""
    try:
        end = end.date()
        end = str(end)
    except:
        end = ""
    
    print contract_end
    try:
        contract_end = contract_end.date()
        contract_end = str(contract_end)
    except:
        #print "Exception 3: CANNOT CONVERT TO DATE""
        contract_end = ""
    
    return start, end, contract_end, tagd, tagt


## DATA PREP
headers = ["id", "start", "end", "contract_end", "tagd", "tagt"]
columns = [idcol, rawtextcol, rawtermscol]

## GET VALUES
model = load_data(file, columns)
#print model

## SAVE VALUES
save_data(model, outfile, headers)
print "job finished."
